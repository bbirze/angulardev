import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Film } from '../film'
import { FilmService } from '../film.service'

@Component({
  selector: 'app-film-detail',
  templateUrl: './film-detail.component.html',
  styleUrls: ['./film-detail.component.css']
})
export class FilmDetailComponent implements OnInit, OnDestroy {

  film!: Film;
  subscriberParams: any;
  
  constructor(private filmService: FilmService, private route: ActivatedRoute) {}
  
  ngOnInit() {
    this.subscriberParams = this.route.paramMap.subscribe((paramMap: any) => {
      let id: number = +paramMap.get('id');   
      this.film = this.filmService.getFilmById(id);
    });
  }

  ngOnDestroy() {
    this.subscriberParams.unsubscribe();
  }
}
