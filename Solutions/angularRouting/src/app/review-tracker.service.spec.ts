import { TestBed } from '@angular/core/testing';

import { ReviewTrackerService } from './review-tracker.service';

describe('ReviewTrackerService', () => {
  let service: ReviewTrackerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ReviewTrackerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
