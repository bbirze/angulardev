export class Film {
    constructor(
        public id: number,
        public title: string,
        public blurb: string,
        public price: number,
        public score: number,
        public genres: Array<string>) {
    }
}
