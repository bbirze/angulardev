import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component'
import { AnotherComponent } from './another/another.component'
import { PagenotFoundComponent } from './pagenot-found/pagenot-found.component'
import { ShoppingcartComponent } from './shoppingcart/shoppingcart.component'

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'another', component: AnotherComponent },
  { path: 'shoppingcart', component: ShoppingcartComponent },
  { path: '**', component: PagenotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
