import { Component, OnInit } from '@angular/core';

class Item {
  constructor (
    public id:number,
    public title:string,
    public quantity:number,
    public price: number) {}
}


@Component({
  selector: 'app-shoppingcart',
  templateUrl: './shoppingcart.component.html',
  styleUrls: ['./shoppingcart.component.css']
})
export class ShoppingcartComponent implements OnInit {
  public items: Array<Item> = [];

  constructor() {
    this.items.push(new Item(0, 'Skis', 4, 250));
    this.items.push(new Item(1, 'iPad', 2, 3000));
    this.items.push(new Item(2, 'Bugatti', 1, 2000000));
    this.items.push(new Item(3, 'Cardiff City shirt', 1, 5));
    this.items.push(new Item(4, 'Swansea City shirt', 20000, 45));
   }

  ngOnInit(): void {}

  remove(id: number)  {
    for (let index=0; index < this.items.length; index++) {
      if (this.items[index].id === id) {
        this.items.splice(index, 1)
      }
    }
  }

}
