import { Component } from '@angular/core';

@Component({
  selector: 'app-film-list-header',
  templateUrl: './film-list-header.component.html',
  styleUrls: ['./film-list-header.component.css']
})
export class FilmListHeaderComponent {
}
