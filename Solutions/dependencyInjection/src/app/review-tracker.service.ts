import { Injectable } from '@angular/core';

@Injectable()
export class ReviewTrackerService {

  verbose: Boolean;

  constructor(verbose: Boolean) {
    this.verbose = verbose;
  }
        
  reviewFilm(filmId: number, comment: string): void {
    let message: string;
    if (this.verbose) {
      message = `[Film ${filmId}] ${comment} [${new Date()}]`;
    }
    else {
      message = `[Film ${filmId}] ${comment}`;
    }
    console.log(message);
  }
}
