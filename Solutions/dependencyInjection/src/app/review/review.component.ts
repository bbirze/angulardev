import { Component, OnInit, Input } from '@angular/core';
import { Film } from '../film'
import { ReviewTrackerService } from '../review-tracker.service';

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.css']
})
export class ReviewComponent {

  @Input() 
  film!: Film;
  
  reviewComments: Array<string> = [];
  reviewComment!: string;

  constructor(private reviewTrackerService: ReviewTrackerService) {}

  submitReview(): void {
    this.reviewComments.push(this.reviewComment);
    this.reviewTrackerService.reviewFilm(this.film.id, this.reviewComment);
  }
}
