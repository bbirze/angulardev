import { Component, OnInit } from '@angular/core';
import { Film } from './film'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  film: Film;
  title = 'Jane Doe';

  constructor()  {}
  ngOnInit(): void {
    this.film = new Film(
      'Spectre',
      'Bond is back in a frantic battle against his classic foe and his white cat',
      8.99);

    this.film.addGenres('action', 'spy');
    this.film.rate(5);
    this.film.rate(4);
    this.film.rate(4);
    this.film.rate(3);

    console.log(this.film.toString())
  }


}
