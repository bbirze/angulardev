import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'csv'
})
export class CsvPipe implements PipeTransform {

    transform(value: any, ...fields: string[]): string {
    let csvString = '';
    for (let f of fields) {
      csvString += value[f] + ','
    }
    csvString = csvString.slice(0, -1);   // remove last ','
    return csvString;
  }
}
