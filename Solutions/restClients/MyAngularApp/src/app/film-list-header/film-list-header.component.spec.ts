import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FilmListHeaderComponent } from './film-list-header.component';

describe('FilmListHeaderComponent', () => {
  let component: FilmListHeaderComponent;
  let fixture: ComponentFixture<FilmListHeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FilmListHeaderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FilmListHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
