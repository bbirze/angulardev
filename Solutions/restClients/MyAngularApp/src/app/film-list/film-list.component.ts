import { Component } from '@angular/core';
import { Film } from '../film';
import { FilmService } from '../film.service';
import { ReviewTrackerService } from '../review-tracker.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-film-list',
  templateUrl: './film-list.component.html',
  styleUrls: ['./film-list.component.css'],

  providers: [
  {
    provide: ReviewTrackerService, 
    useFactory: (verbose: Boolean) => new ReviewTrackerService(verbose),
    deps: ["IS_VERBOSE_LOGGER"]
  }]
})
export class FilmListComponent {

  films: Observable<Array<Film>>; 

  constructor(filmService: FilmService) { 
    this.films = filmService.getFilms(); 
  }
}
