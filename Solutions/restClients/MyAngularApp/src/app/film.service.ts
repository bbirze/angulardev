import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Film } from './film'

@Injectable({
  providedIn: 'root'
})
export class FilmService {

  private baseUrl: string = 'http://localhost:8080/api/films';

  constructor(private http: HttpClient) {} 

  getFilms(): Observable<Array<Film>>  {
      return this.http.get(this.baseUrl) as Observable<Array<Film>>;
  }

  getFilmById(id: number): Observable<Film> {
      return this.http.get(`${this.baseUrl}/${id}`) as Observable<Film>;
  }
}
