import { Component } from '@angular/core';
import './film'
import './filmOptional'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Jane Doe';
}
