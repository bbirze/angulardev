// JavaScripts data types
// held in values, not variables
// -----------------------------

let a = 'string var'
let b = true  // boolean value
let c = 24    // numeric value

a = b         // ok, type held in value

// No type checking in arrays
// ---------------------------
let color = ['red', 'white' , 'square', true]
let numbers = [18, 24, 56, true]

function sum(x, y, z) {
  return x + y + z;
}

console.log(sum(...numbers));
// expected output: 98

console.log(sum.apply(null, numbers));
// expected output: 98
