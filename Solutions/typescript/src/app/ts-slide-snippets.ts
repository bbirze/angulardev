/* *******************************
 * Copy and paste any of these snippets into the
 * TypeScript playground to run them
 *
 * https://www.typescriptlang.org/play
 *
 * *******************************/

// Define Types in Declarations
// -----------------------------
var a:string = 'string var'  // string value
let b:boolean = true         // boolean value
let c: number = 24            // numeric value
let d: any = 'hello'

a = b               // compile error
let s: string = 10  // compile error
d = c               // ok, any type held in value


// Type checking in arrays
// ---------------------------
                                            // true not a string
let color: string[] = ['red', 'white' , 'square', true]
                                            // 'Yes!' not a number
let numbers: number[] = [18, 24, 56, 'Yes!']

let goosey: any[] = [18, 24, 56, true]      // ok

function sum(x, y, z) {
  return x + y + z;
}
console.log(sum(...numbers));               // expected output: 98
console.log(sum.apply(null, numbers));      // expected output: 98

// Enumeration and Custom Type Arrays
// ----------------------------------
enum Color { Red, Green, Blue, White }

let good: Color[] = [ Color.Red, Color.Blue, Color.White]

let bad: Color[] = [ Color.Red, Color.Blue,
                     Color.Purple,   // Purple not in Color Enum
                     Pink ]          // Pink undefined

// Function strict typing
// ---------------------------
function sayHi(name: string): string {
  return `Hi ${name}!`
}

let g1 = sayHi('Benny')          // OK
let g2 = sayHi(10)               // 10 not a string
let g3:number = sayHi('Benny')   // g3 not a string


// Compiler Type Inferance
// ---------------------------
var v1 = 'hi!'      // v1 is a string
v1 = 288            // 88 is not a string

let an            // uninitialized variables are any type
an = 'greetings'  // ok
an = 288          // also ok

// Compiler Function Type Inferance
// --------------------------------
function sayHello(name) {          // arg type inferred
  return `Hi ${name}!`           // return type inferred
}

let g4 = sayHello('Benny')          // g1 type inferred
let g5:number = sayHello('Benny')   // g3 not a string


// Default parameter values
// ---------------------------
function calcSalary(basic:number,
                  bonus = 0.0,
                  director = false) {

 let earnings = basic + bonus;
 if (director)   { earnings *= 2;  }

 return earnings;
}

var t1  = calcSalary(40000, 500)             // return 40500
var t2  = calcSalary(40000, undefined, true) // return 80000

// Indicate Optional Parameters with ‘?’
// -------------------------------------
function calcSalaryOpt(basic: number,
                       bonus = 0.0,
                       director = false,
                       offshoreSlushFund?: number) {   // optional
  let earnings: number = basic + bonus;

  if (offshoreSlushFund) {                // undefined if not given
    earnings += offshoreSlushFund;
  }
  if (director)   { earnings *= 2;  }

  return earnings;
}

var t3  = calcSalaryOpt(40000, undefined, true)      // return 80000
var t4  = calcSalaryOpt(40000, undefined, true, 500) // return 81000

// Arrow Functions in TypeScript
// -------------------------------------
let list = [0, 1, 2, 3, 4, 5]

let evenNum:number[] = list.filter(n => {
  if (n % 2 === 0) {
    return true
  } else {
    return false
  }
})
console.log(evenNum)   // [0, 2, 4]

// Template Strings TypeScript
// -------------------------------------
let nam1 = 'Roger'
let g6 = 'Hello ' + nam1 + '!'  // concatenated string
let g7 = `Hello ${nam1}!`       // template string

console.log(                    // multiline template strings
  `normal: ${g6}
   template: ${g7}`)
/* output :
   normal: Hello Roger!
   template: Hello Roger!"
*/

// Inheritance
// -------------------------------------
class Animal {
  name: string;
  constructor(name: string) {
    this.name = name
  }
}

class Cat extends Animal {

  constructor(petName: string, public sound: string) {
     super(petName)
  }
  purr() { return this.sound }
}

let ct = new Cat('Fluffy', 'rrrrrrrrr')
console.log(`${ct.name} sounds like ${ct.purr()}.`);

let animal: Animal = ct;  // upcast to animal
if (animal instanceof Cat) {
  let c: Cat = animal as Cat
  c.purr()
}

