import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AnotherComponent } from './another/another.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { ShoppingcartComponent } from './shoppingcart/shoppingcart.component';
import { CsvPipe } from './csv.pipe';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AnotherComponent,
    PagenotfoundComponent,
    ShoppingcartComponent,
    CsvPipe
  ],
  imports: [
    FormsModule,
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
