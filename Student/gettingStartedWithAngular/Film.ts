class Film {

    private static nextId: number = 0;
    public id: number;
    public genres: string[] = [];
  
    constructor(
        public title: string,
        public blurb: string,
        public price: number,
        public score: number) { 
            this.id = Film.nextId++;
    }
  
    addGenre(...genres: string[]) : void {
        for (let genre of genres) {
            this.genres.push(genre);
        }
    }
  
    genresAsString() : string {
        return this.genres.join(' ');
    } 
}
