import { Component } from '@angular/core';

class Item {
  constructor(
    public id: number, 
    public title: string, 
    public quantity: number, 
    public price: number,
    public timestamp: Date = new Date()) 
    {}
}

@Component({
  selector: 'app-shoppingcart',
  templateUrl: './shoppingcart.component.html',
  styleUrls: ['./shoppingcart.component.css']
})
export class ShoppingcartComponent {

  items: Array<Item> = [];
    
  constructor() {
    this.items.push(new Item(0, 'Skis', 4, 250));
    this.items.push(new Item(1, 'iPad', 2, 3000));
    this.items.push(new Item(2, 'Bugatti', 1, 2000000));
    this.items.push(new Item(3, 'Cardiff City shirt', 1, 5));
    this.items.push(new Item(4, 'Swansea City shirt', 20000, 45));
  }

  // Find the array index of the item with the specified id, or -1 if not found.
  private findItem(id: number) : number {
    for (var i = 0; i < this.items.length; i++) {
      if (this.items[i].id === id) {
        return i;
      }
    }
    return -1;
  }

  // Remove the item with the specified id.
  remove(id: number) {
    let index = this.findItem(id);
    if (index != -1) {
      this.items.splice(index, 1);
    }
  }
  
  // Calculate the line cost for the item with the specified id.
  lineCost(id: number) : number {
    let index = this.findItem(id);
    if (index != -1) {
      let item = this.items[index];
      return item.price * item.quantity
    }
    else {
      return 0;
    }
  }
  
  // Calculate the total cost of all lines in the cart.
  totalCartCost() : number {
    let total = 0;
    for (let item of this.items) {
      total += item.price * item.quantity;
    }
    return total;
  }
}
