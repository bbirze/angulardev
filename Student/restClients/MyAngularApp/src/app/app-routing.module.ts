import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component'
import { AnotherComponent } from './another/another.component'
import { ShoppingcartComponent } from './shoppingcart/shoppingcart.component'
import { FilmListComponent } from './film-list/film-list.component'
import { FilmDetailComponent } from './film-detail/film-detail.component'
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component'

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'another', component: AnotherComponent },
  { path: 'shoppingcart', component: ShoppingcartComponent },
  { path: 'filmlist', component: FilmListComponent },
  { path: 'filmDetail/:id', component: FilmDetailComponent}, 
  { path: '**', component: PagenotfoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
