import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AnotherComponent } from './another/another.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { ShoppingcartComponent } from './shoppingcart/shoppingcart.component';
import { CsvPipe } from './csv.pipe';
import { FilmListComponent } from './film-list/film-list.component';
import { FilmListHeaderComponent } from './film-list-header/film-list-header.component';
import { FilmListFooterComponent } from './film-list-footer/film-list-footer.component';
import { FilmItemComponent } from './film-item/film-item.component';
import { ReviewComponent } from './review/review.component';
import { FilmDetailComponent } from './film-detail/film-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AnotherComponent,
    PagenotfoundComponent,
    ShoppingcartComponent,
    CsvPipe,
    FilmListComponent,
    FilmListHeaderComponent,
    FilmListFooterComponent,
    FilmItemComponent,
    ReviewComponent,
    FilmDetailComponent,
  ],
  imports: [
    FormsModule,
    BrowserModule,
    AppRoutingModule
  ],
  providers: [{provide: "IS_VERBOSE_LOGGER", useValue: true}],
  bootstrap: [AppComponent]
})
export class AppModule { }
