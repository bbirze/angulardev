import { Component, OnInit, Input } from '@angular/core';
import { Film } from '../film'

@Component({
  selector: 'app-film-item',
  templateUrl: './film-item.component.html',
  styleUrls: ['./film-item.component.css']
})
export class FilmItemComponent implements OnInit {

  @Input() 
  film!: Film;

  imageUrl!: string;

  ngOnInit() {
      this.imageUrl = `assets/${this.film.id}.jpg`;
  }
}
