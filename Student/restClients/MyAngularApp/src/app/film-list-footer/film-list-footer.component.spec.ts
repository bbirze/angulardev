import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FilmListFooterComponent } from './film-list-footer.component';

describe('FilmListFooterComponent', () => {
  let component: FilmListFooterComponent;
  let fixture: ComponentFixture<FilmListFooterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FilmListFooterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FilmListFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
