import { Component } from '@angular/core';

@Component({
  selector: 'app-film-list-footer',
  templateUrl: './film-list-footer.component.html',
  styleUrls: ['./film-list-footer.component.css']
})
export class FilmListFooterComponent {
  timestamp = new Date()
}
