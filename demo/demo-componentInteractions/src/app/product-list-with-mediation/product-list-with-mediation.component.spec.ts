import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductListWithMediationComponent } from './product-list-with-mediation.component';

describe('ProductListWithMediationComponent', () => {
  let component: ProductListWithMediationComponent;
  let fixture: ComponentFixture<ProductListWithMediationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProductListWithMediationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductListWithMediationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
