import { Component } from '@angular/core';
import { ProductService } from '../product-service'
import { Product } from '../product'
import { ISale } from '../product-item-with-sales/product-item-with-sales.component'

@Component({
  selector: 'app-product-list-with-mediation',
  templateUrl: './product-list-with-mediation.component.html',
  styleUrls: ['./product-list-with-mediation.component.css']
})
export class ProductListWithMediationComponent  {

  products: Array<Product> = []; 
  newSale!: ISale;

  constructor() { 
      let productService = new ProductService();
      this.products = productService.getProducts(); 
  }

  onSale(event: ISale) {
      this.newSale = event;
  }  
}
