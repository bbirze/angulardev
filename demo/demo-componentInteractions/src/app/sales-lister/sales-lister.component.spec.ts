import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesListerComponent } from './sales-lister.component';

describe('SalesListerComponent', () => {
  let component: SalesListerComponent;
  let fixture: ComponentFixture<SalesListerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SalesListerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesListerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
