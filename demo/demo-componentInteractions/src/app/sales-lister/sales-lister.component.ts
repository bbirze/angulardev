import { Component, Input } from '@angular/core';
import { ISale } from '../product-item-with-sales/product-item-with-sales.component'

@Component({
  selector: 'app-sales-lister',
  templateUrl: './sales-lister.component.html',
  styleUrls: ['./sales-lister.component.css']
})
export class SalesListerComponent {

  sales: Array<string> = [];

  private _newSale!: ISale;

  @Input() 
  set newSale(value: ISale) {
      if (value) {
        let msg:string = value.productDescription + ' [' + value.quantity + ']';
        this.sales.push(msg);
      }
  }

  get newSale(): ISale {
      return this._newSale;
  }

}
