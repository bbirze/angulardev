import { Component } from '@angular/core';

class Car {
  constructor(public make: string, public model: string) {}
}

@Component({
  selector: 'app-data-binding-syntax',
  templateUrl: './data-binding-syntax.component.html',
  styleUrls: ['./data-binding-syntax.component.css']
})
export class DataBindingSyntaxComponent {
  firstName: string = 'Ola';
  lastName: string = 'Nordmann';
  nationality: string = 'Norsk';
  emailAddress: string = 'ola.nordmann@mydomain.com';
  companyCar: Car;
  salary: number = 55000;
  verbose: boolean = true;
  days: number = 365;
  years: number = 2;

  constructor() {
    this.companyCar = new Car('Bugatti', 'Chiron');
  }

  label() {         // text in button
    return (this.verbose) ? 'Show brief details' : 'Show verbose details';
  }

  onVerboseToggle(event: any) {
    this.verbose = !this.verbose;
    console.log('Event handler invoked on element ' + event.target);
  }
}
