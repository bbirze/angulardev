import { Component } from '@angular/core';

@Component({
  selector: 'app-ng-for',
  templateUrl: './ng-for.component.html',
  styleUrls: ['./ng-for.component.css']
})
export class NgForComponent {

  firstName: string  = 'Ola';
  lastName: string = 'Nordmann';
  nationality: string = 'Norsk';
  emailAddress: string = 'ola.nordmann@mydomain.com';
  salary: number = 25000;
  skills: Array<string> = ['JavaScript', 'Angular', 'C#', 'Java'];;

  constructor() {}

  payRise() {
    this.salary = this.salary + 5000;
  }
}
