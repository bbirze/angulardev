import { Component } from '@angular/core';

@Component({
  selector: 'app-ng-if-then-else',
  templateUrl: './ng-if-then-else.component.html',
  styleUrls: ['./ng-if-then-else.component.css']
})
export class NgIfThenElseComponent {

  firstName: string  = 'Ola';
  lastName: string = 'Nordmann';
  nationality: string = 'Norsk';
  emailAddress: string = 'ola.nordmann@mydomain.com';
  salary: number = 25000;
  skills: Array<string> = ['JavaScript', 'Angular', 'C#', 'Java'];;

  constructor() {}

  payRise() {
    this.salary = this.salary + 5000;
  }
}
