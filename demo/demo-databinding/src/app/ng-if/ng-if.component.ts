import { Component } from '@angular/core';

@Component({
  selector: 'app-ng-if',
  templateUrl: './ng-if.component.html',
  styleUrls: ['./ng-if.component.css']
})
export class NgIfComponent {

  firstName: string  = 'Ola';
  lastName: string = 'Nordmann';
  nationality: string = 'Norsk';
  emailAddress: string = 'ola.nordmann@mydomain.com';
  salary: number = 25000;
  skills: Array<string> = ['JavaScript', 'Angular', 'C#', 'Java'];;

  constructor() {}

  payRise() {
    this.salary = this.salary + 5000;
  }
}
