import { Component } from '@angular/core';

@Component({
  selector: 'app-ng-switch',
  templateUrl: './ng-switch.component.html',
  styleUrls: ['./ng-switch.component.css']
})
export class NgSwitchComponent {

  firstName: string  = 'Ola';
  lastName: string = 'Nordmann';
  nationality: string = 'USA';
  emailAddress: string = 'ola.nordmann@mydomain.com';
  salary: number = 25000;
  skills: Array<string> = ['JavaScript', 'Angular', 'C#', 'Java'];;

  constructor() {}

  payRise() {
    this.salary = this.salary + 5000;
  }
}
