import { Component } from '@angular/core';

@Component({
  selector: 'app-two-way-binding',
  templateUrl: './two-way-binding.component.html',
  styleUrls: ['./two-way-binding.component.css']
})
export class TwoWayBindingComponent {

  firstName: string  = 'Ola';
  lastName: string = 'Nordmann';
  nationality: string = 'Norsk';
  emailAddress: string = 'ola.nordmann@mydomain.com';
  salary: number = 25000;

  constructor() {}

  payRise() {
    this.salary = this.salary + 5000;
  }

  onSalaryChange($event: any) {
    this.salary = Number($event.target.value);
  }
}
