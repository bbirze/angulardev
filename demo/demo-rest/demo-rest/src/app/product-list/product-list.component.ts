import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product.service'
import { Product } from '../product'
import { Observable } from 'rxjs';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  productsTechnique1!: Observable<Array<Product>>;
  productsTechnique2!: Array<Product>;

  constructor(private productService: ProductService) {}

  ngOnInit(): void {
    // Get products, technique 1 -
    // just assign the Observable result directly.
    //     use async pipe in HTML Template to consume Observable
    this.productsTechnique1 = this.productService.getProducts();

    // Get product, technique 2 - subscribe to the service.
    // New inputs will change productsTechnique2,
    //     triggering a change event for re-rendering
    this.productService.getProducts().subscribe({
      next: (data:any) => this.productsTechnique2 = data,
      error: (_:any)  => console.log("Error")
    });
  }
}
