export class Product {
    constructor(
        public id: number, 
        public description: string, 
        public category: string, 
        public price: number) 
        {}
}
  