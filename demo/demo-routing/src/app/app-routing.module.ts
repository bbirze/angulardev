import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component'
import { RoutingParamsAndDataComponent } from './routing-params-and-data/routing-params-and-data.component'
import { ProductItemComponent } from './product-item/product-item.component'
import { MasterDetailComponent } from './master-detail/master-detail.component'

const routes: Routes = [
  { path: '', component: HomeComponent },

  { path: 'routing-params-and-data', component: RoutingParamsAndDataComponent },
  { path: 'products/:id', component: ProductItemComponent, data: {backLink: '/routing-params-and-data'} },

  { path: 'master-detail', component: MasterDetailComponent,
    children: [
      { path: 'products/:id',
        outlet: 'masterDetailRouterOutlet',
        component: ProductItemComponent,
        data: {backLink: undefined}
      },
  ]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
