import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { MasterDetailComponent } from './master-detail/master-detail.component';
import { ProductItemComponent } from './product-item/product-item.component';
import { RoutingParamsAndDataComponent } from './routing-params-and-data/routing-params-and-data.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    MasterDetailComponent,
    ProductItemComponent,
    RoutingParamsAndDataComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
