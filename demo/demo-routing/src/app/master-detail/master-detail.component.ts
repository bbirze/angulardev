import { Component } from '@angular/core';
import { Product } from '../product'
import { ProductService } from '../product.service'

@Component({
  selector: 'app-master-detail',
  templateUrl: './master-detail.component.html',
  styleUrls: ['./master-detail.component.css']
})
export class MasterDetailComponent {

  products: Array<Product> = []; 

  constructor(productService: ProductService) { 
      this.products = productService.getProducts(); 
  }
}
