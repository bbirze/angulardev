import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Product } from '../product';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.css']
})
export class ProductItemComponent implements OnInit, OnDestroy {

    product!: Product;
    backLink!: string;

    subscriberParams: any;      // holds subscription to route parameters
    subscriberData: any;        // holds subscription to fixed data backLink

    constructor(private productService: ProductService, private route: ActivatedRoute) {}

    ngOnInit() {
        console.log("ProductItemComponent instantiated at " + new Date());

        this.subscriberParams = this.route.paramMap.subscribe((paramMap : any) => {
            let id: number = +paramMap.get('id');   // (+) converts string 'id' to a number
            this.product = this.productService.getProductById(id);
        });

        this.subscriberData = this.route.data.subscribe(data => {
            this.backLink = data['backLink'];
        });
    }

    ngOnDestroy() {
        console.log("ProductItemComponent destroyed at " + new Date());
        this.subscriberParams.unsubscribe();
        this.subscriberData.unsubscribe();
    }
}
