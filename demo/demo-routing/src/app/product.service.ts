import { Injectable } from '@angular/core';
import { Product } from './product'

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private products: Array<Product>;
  
  constructor() {
      this.products = [
          new Product(0, 'Swansea City shirt', 'Leisure wear', 45),
          new Product(1, 'Cardiff City shirt', 'Leisure wear', 5),
          new Product(2, 'Bugatti Chiron', 'Auto', 2000000),
          new Product(3, '65 inch UHDTV', 'TV/Audio', 1800),
          new Product(4, 'Carving skis', 'Sports equipment', 350),
          new Product(5, 'Ski boots', 'Sports equipment', 150)
      ];
  }

  getProducts(): Array<Product> {
      return this.products;
  }

  getProductById(id: number): Product {
      return this.products[id];
  }
}
