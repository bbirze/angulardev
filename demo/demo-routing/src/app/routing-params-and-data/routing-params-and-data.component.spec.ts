import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RoutingParamsAndDataComponent } from './routing-params-and-data.component';

describe('RoutingParamsAndDataComponent', () => {
  let component: RoutingParamsAndDataComponent;
  let fixture: ComponentFixture<RoutingParamsAndDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RoutingParamsAndDataComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RoutingParamsAndDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
