import { Component } from '@angular/core';
import { Product } from '../product'
import { ProductService } from '../product.service'

@Component({
  selector: 'app-routing-params-and-data',
  templateUrl: './routing-params-and-data.component.html',
  styleUrls: ['./routing-params-and-data.component.css']
})
export class RoutingParamsAndDataComponent {

  products: Array<Product> = []; 

  constructor(productService: ProductService) { 
      this.products = productService.getProducts(); 
  }
}
