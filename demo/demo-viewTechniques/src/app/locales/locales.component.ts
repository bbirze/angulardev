import { Component } from '@angular/core';

@Component({
  selector: 'app-locales',
  templateUrl: './locales.component.html',
  styleUrls: ['./locales.component.css']
})
export class LocalesComponent {

  rating: number = 0.9874754654;;
  salary: number = 1500000.99;
  timestamp: Date = new Date();

  selectedLocale!: string;

  constructor() {}

}
