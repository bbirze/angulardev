import { Component } from '@angular/core';

@Component({
  selector: 'app-simple-pipes',
  templateUrl: './simple-pipes.component.html',
  styleUrls: ['./simple-pipes.component.css']
})
export class SimplePipesComponent {

  name: string= 'Michu';;
  team: string= 'swansea city';
  rating: number = 0.9874754654;;
  salary: number = 1500000.99;
  email: string = 'michu@example.com';
  timestamp: Date = new Date();
  additionalInfo: any = {
          nationality: 'Spain',
          age: 35,
          height: 1.83,
          car: 'Bugatti'
        };

  constructor() {}
}
