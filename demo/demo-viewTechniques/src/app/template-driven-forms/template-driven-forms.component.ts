import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';    // must import to use in submit handler

// Define a class to represent a category.
class Category {
  constructor(public id?: number,
              public name?: string) {}
}

// Define a class to represent a single product.
// The ? means the field is optional, doesn't need a value
//
class Product {
  constructor(
    public description?: string,
    public category?: string,
    public categoryAsObject?: Category,
    public supplierEmail?: string,
    public price?: number,
    public additionalInfo?: string
  ) {}
}

// Define an Angular component to display a product in a form.
@Component({
  selector: 'app-template-driven-forms',
  templateUrl: './template-driven-forms.component.html',
  styleUrls: ['./template-driven-forms.component.css']
})
export class TemplateDrivenFormsComponent {

  categories = ['Clothing', 'Audio/Video', 'Auto', 'Food/Drink'];

  categoriesAsObjects: Array<Category> = [
    new Category(0, 'Clothing'),
    new Category(1, 'Audio/Video'),
    new Category(2, 'Auto'),
    new Category(3, 'Food/Drink')
  ];

  model = new Product();            // hold user product input
  products: Array<Product> = [];

  // select [compareWith] function, similar to trackBy method for *ngFor
  // used to track identities to know when to re-render
  //
  categoryById(c1: Category, c2: Category) {
    return c1 && c2 && c1.id == c2.id;
  }

  onSubmit(form: NgForm) {
    this.products.push(this.model);

    console.log(`Price is: ${this.model.price}`);
    console.log(`Price is: ${form.value.price}`);

    form.reset();
    this.model = new Product();     // clear model for next product input
  }
}
